let c;
var clouds = [];
let sun;
let button;
let showCloud = false;
let gameOver = false;
var cloudAni;


function preload() { //adding animated cloud and animated sun to program
cloudAni = loadAnimation('assets/cloud_pulsing0001.png','assets/cloud_pulsing0007.png');
sun = loadAnimation('assets/sun1.png','assets/sun3.png');
}

function setup() {
  c = createCanvas(600,500);
  c.position(0,0);
  background('#58D3F7');

  button = createButton('Avoid clouds - click here to start');
    button.size(60,60);
    button.position(300,250);
    button.show();
    button.style('font-size', '10px');
    button.style('background-color', 'white');
    button.style('color', 'black');

    for(var i=0; i<4; i++){
      clouds[i]=(new Cloud(floor(random(3,5)),floor(random(width)),0,floor(random(10,10))));
    }
  }

function draw() {
  background('#58D3F7');

    animation(sun,mouseX,mouseY);

    if (showCloud) {

      for (let i=0; i < clouds.length; i++){
        clouds[i].move();
        clouds[i].show();
        if(dist(mouseX,mouseY,clouds[i].x,clouds[i].y) <100) {
            gameOver=true;

        }
    }
    if (gameOver==true) {
      fill(255,0,0);
      textSize(25);
      text('TOO BAD - IT IS NO LONGER A SUNNY DAY',50,300);
      noLoop();
    }
  }

    button.mousePressed(start);
}

function start() {
  button.hide();
  showCloud=true;
  }

class Cloud  {
  constructor(speed,xpos,ypos,size) {
    this.speed = speed;
    this.x=xpos;
    this.y=ypos;
    this.size = size;
  }
  move() {
    this.y += this.speed;
    if (this.y > height+5) {
    this.y = 0;
    this.x = random(floor(random(width)))
    }
  }
  show() {
    animation(cloudAni,this.x, this.y);
  }
}
