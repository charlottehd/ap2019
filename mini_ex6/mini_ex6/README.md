# On a lovely sunny day

![Screenshot](sunnyday.PNG)



Link
https://cdn.staticaly.com/gl/charlottehd/ap2019/raw/master/mini_ex6/mini_ex6/empty-example/index.html



My game is called "On a lovely sunny day" and it is mainly about avoiding clouds. You control the sun by using your mouse and the task is to find your way on the light blue sky without touching any of the clouds. If you do touch a cloud the game is over and the text "Too bad - it is no longer a sunny day" will appear on the screen. I added a button to my game which allows the player to choose when he/she wants to start, so the game doesn't start immediately when opening the link. 

I found this mini exercise extremely difficult - perhaps the one that took me the longest to code. My problems first occured when I wanted clouds to randomly fall down from top of the screen. I created a class named Clouds and used a various of syntaxes - for example the [cloud.push (new Clouds...)] which made it possible for me to make the clouds position and speed random. The next obstacle - which took me around 5 hours - was how do I make the game stop when the sun touches the clouds? First I tried a collide function, but since my animations isn't sprites, that one wasn't possible. I ended up getting help and used a dist. function instead. 
