# Flowcharts

## Individual flowchart

![Screenshot](flow3.PNG)

When looking back at my old programs the one that I found the most fun to code was in the theme object oriented programming (OOP). For that reason I chose to make a flowchart that describes my game from OOP week. I found that it didn't have many options and therefor my flowchart ended up being quite simple. I kinda like that actually, because it shows how simple a program can be visualised in a flowchart even though it has some complicated syntax in the code.

## Group flowchart

### 1
![Screenshot](flow1.png) 

### 2
![Screenshot](flow2.png)

For this week we were assigned to develop two flowcharts in the group. In this phase the point was not to come up with super realistic ideas or super technical ideas either. The point was to try to create flowcharts without having written the code yet. Thus it should be more simple for us when we start coding, since the program is already sketched and divided into steps. 

A challenge will be for us to carry out our ambitions for these flowcharts. Especially the flowchart of the game will have several obstacles along the way. First of all we will probably face difficulties when creating our avatar, as we wish to capture a picture of the user so the user becomes the avatar in the game. To make this happen we must make use of our knowledge from the theme of data capturing using the p5.dom library. We must use a face tracker and identify which parts of the face we want to incorporate in the avatar. It is important that the face moves along with the avatar throughout the whole game.
Another challenge will be to make a magnetic space between the “star”, which symbolises the expectations we meet during our youth, and the avatar, pulling the avatar towards the star. We will try to solve this by making a transparent square around the star and if the avatar is inside this square it will hit the expectations and you will lose happiness in the “happy-meter”. There is some sort of attractive force. This force will make it more difficult for you to reach your life-goals and gain happiness, since expectations will pull you over wherever you go in the arena and lower your happiness if you hit it.
It might also be quite a challenge for us to create a “happy-meter” which keeps lowering when you don’t reach life-goals. We want to lower it by approximately 5% for every five seconds you don’t collect a life-goal to make it more realistic and similar to real life. 

Our second flowchart shows a more simple  program containing parts we have worked with before. The main challenge will be to continuously query a new word without having to write them over and over again. Also this word has to match with a gif from the API which in this case is GIPHY. Another challenge is how the ball should move around and we will try solving this by looking at one of our colleagues’ codes, and hopefully we can use the same method.


The biggest difference from making my own individual flowchart and making the group ones, is that when making mine I made it based on a program that already existed and the group flowcharts are new programs that hasn't been coded yet. I feel like you can see a clear difference in the two sorts of flowcharts shown above. When you come up with a flowchart at first you often tempt to be a bit unrealistic or at least have high ambitions towards what program you will end up making. It may sometimes provoke new challenges, which is often a good thing. But other times these challenges or obstacles are more than what you can conquer and then the flowchart doesn't show what the program will really be like. For the same reason i think my own individual flowchart is a bit boring and not very explorative, since the program that I made, shows what abilities I had at the time.

