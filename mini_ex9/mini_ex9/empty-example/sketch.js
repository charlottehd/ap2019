//New York Times API:
var api = "https://api.nytimes.com/svc/search/v2/articlesearch.json?q="
var apikey = "&api-key=25FYAXiAvgrojsGkwTc2Cd6K7yAg8Z2W"
//giphy API:
var api1 = "http://api.giphy.com/v1/gifs/search?";
var apikey1 = "&api_key=inl6GHATA669CZKYgoEPHXlGWV5WO49x&q=";

//loading of the news font
function preload(){
  myFont = loadFont('HomenuliShadow.ttf');
}

function setup() {
  createCanvas(800,1300);
  background(240);

}

function draw(){
  back(); //displays the newpaper
  //making of the search space
  var input = select('#word');
  input.position(500,165);

  //making of the search button
  var button = select('#submit');
  button.mousePressed(newArticle);
  button.position(630, 165);


//loading JSON files from New York Times and Giphy.com
  function newArticle(){
    back();// displays the newspaper on top of the old when you search
    var url = api + input.value() + apikey;
    var url1 = api1 + apikey1 + input.value();
  // the input.value is the query word that is written in the search bar
    loadJSON(url, gotData);
    loadJSON(url1, gotData1);
    }

  //the making of the news paper-like background
  function back(){
    fill('white');
    noStroke();
    rect(100,100,600,1200);

    noFill();
    stroke(0);
    strokeWeight(0.5);
    rect(120,240,560,1000);

    strokeWeight(5);
    line(120,220,680,220);

    fill('black');
    noStroke();
    textFont(myFont);
    textSize(66);
    textStyle(BOLD);
    text("BEST DAILY NEWS TODAY", 120, 190);
  }

//Loads the news article from New York Times JSON.
  function gotData(data){
      var randomnum = floor(random(10)); //combines the header and lower too make sure they choose the same news article. Random so it doesn't always choose the same.
      var articles = data.response.docs;
      var header = articles[randomnum].headline.main;//Divides the article into a header and a lower part to be able to position them where we want
      textFont('Georgia');
      push()
      textStyle(BOLD)
      textSize(35);
      text(header,140,280,540,500);
      pop()
      var lower = articles[randomnum].snippet;
      textSize(20);
      text(lower,140,890,540,500);
  }
//loads a random gif from the giphy.com JSON file among the 10 first gifs of the array
  function gotData1(giphy){
   var img = createImg(giphy.data[floor(random(10))].images.original.url); //chooses a gif among the first 10 pictures within the search word.
   img.position(200,500);
   img.size(400,400);
   }
noLoop(); //draw news article and gif only once for every search word
}
