let load = ['I','will','never','stop','loading'];

function setup() {
  createCanvas(windowWidth,windowHeight);
  background(220,180,240);
  frameRate(2);
}

function draw() {
  background(220,180,240,80);
  fill(220,180,240,120);
  noStroke();
	rect(0,0,width,height);
  textThrobber(9);
}

function textThrobber(num) {
  push();
  translate(width/2, height/2);

 let cir = 360/num*(frameCount%num);

  rotate(radians(cir));
  noStroke();
  textSize(22);
  fill(255);


  text(load[frameCount%load.length],20,0);
	pop();
}
