function setup() { //starter funktionen
  createCanvas(600,600); //definerer størrelsen på canvas
  background(225,180,240); //definerer baggrundsfarven på canvas
  loop();
}

function draw() {
	textSize(60); //definerer tekststørrelsen
  textAlign(CENTER); //definerer, hvor på canvas teksten skal stå
  fill(random(225), random(180), random(240)); //definerer, hvilken farve teksten skal have
	text('PAUSE', random(600), random(600)); //definerer hvad testen skal være, samt hvor teksten skal placeres
  textStyle(BOLD); //definerer teksttypen

  if (mouseIsPressed) { //når man klikker med musen, starter funktionen forfra
    background(225,180,240);
   }
   print(mouseIsPressed);
}
