![Screenshot](emoji.PNG)

https://cdn.staticaly.com/gl/charlottehd/ap2019/raw/master/mini_ex2/p5/empty-example/index.html


# Reflection on mini_ex2

In this exercise i wanted to continue using some of the same elements from my first exercise creating sort of a pattern. I wished to implement words in my emoji, and this is how it turned out.
The task was to create two emojis and to experiment with shapes. In that sense I made a simple ellipse with a stroke but no fill to form the base of my emoji. Instead of making eyes, nose and mouth I used text syntaxes and placed a text where the eye was supposed to be and so on. In the very beginning of my code I declared 3 variables because I wanted the words to change continuosly in a random order. I used a new syntax called 'array' to list up all the words I wanted the 'random'-function to choose between. I used a 'frameRate' syntax to control how fast the words change, because I wanted time to actually read the words before they change. Because the task was to create 2 emojis I added an extra feature to my code. Now if you press the mouse button you will see that that random changing of words pauses as long as you press the mouse. Now the words on the emoji is actually mouth, nose and eye. To make this happen I made use of the 'mouseIsPressed' function.

The reason for me to make this exact emoji is simple. I wished to make an emoji that didn't have any specific eye color or any specific mouth size and so on. I wanted my emoji to be as diverse as possible and to represent that there is not one way of describing an eye or one look of an eye but so many different ways/looks. This emoji represents all people with 2 eyes, one nose and one mouth. 
The words I chose to use are actually all synonyms which I found in a synonym library. This means that the where the nose is supposed to be is all synonyms of the word nose, for example: 'horn','snuffer','muzzle' and more. The same occurs with eye and mouth.
Now when you press the mouse you see the words that are actually what should have been there; eye, nose and mouth. By adding this you can say that in the end it doesn't really matter how we look, cause we are all the same.

An emoji is just an emoji that sometimes can replace words for people to express themselves. Often people debate for their rights because the might feel misrepresented as we've seen with emojis. In the text "Modifying the universal" this exact theme is discussed and the reader learns how emojis have developed over the last decades. One of my main takeaways from the text is that no matter how many emojis are developed, people will never be completely satisfied, cause they will just find something new to complain about.