var eye = ['eye', 'blinder', 'headlight','ocular','optic','pie','eyeball','lamp','peeper',];
//var srcPosition = 4;
//var length = 2;

var nose = ['nose','horn','beak','snoot','snuffer','muzzle'];
var mouth = ['mouth','kisser','orifice','portal','trap','entrance'];

function setup() {
  createCanvas(400,400);
  background(225,180,240);
}

function draw() {
	background(255,180,240);

	frameRate(4);

	ellipseMode(CENTER);
	strokeWeight(8);
	stroke(255,255,255);
	noFill();
	ellipse(200,200,380,380);

	strokeWeight(1);
	textSize(40);
	fill(255,255,255);
	text(random(mouth), 145,320,125,320);

	textSize(40);
	fill(255,255,255);
	text(random(eye), 80,120);

	textSize(40);
	fill(255,255,255);
	text(random(eye), 240,120);


	textSize(40);
	fill(255,255,255);
	text(random(nose), 150,220);



  if (mouseIsPressed) {
  background(255,180,240);

	ellipseMode(CENTER);
	strokeWeight(8);
	stroke(255,255,255);
	noFill();
	ellipse(200,200,380,380);

	strokeWeight(1);
	textSize(40);
	fill(255,255,255);
	text('mouth', 145,320,125,320);

	textSize(40);
	fill(255,255,255);
	text('eye', 80,120);

	textSize(40);
	fill(255,255,255);
	text('wink', 240,120);
	textSize(40);
	fill(255,255,255);
	text('nose', 150,220);

   }

	print(mouseIsPressed);
}
