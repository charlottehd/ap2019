let c;
let button;
let capture;
let webcamOn = false;
let text1;
let text2;
let text3;

function setup() {
  c= createCanvas(windowWidth,windowHeight);
  c.position(0,0);

    capture = createCapture();
    capture.size(width,height);
    capture.position(0,0);
    capture.hide();

    frameRate(3);
	background(220,180,245);
	button = createButton('Do not click me');

    button.size(200,200);
    button.show();
    button.style('font-size', '40px');
    button.style('background-color', 'yellow');
    button.style('color', 'green');

    text1=createDiv('Dont push this button!!');
    text2=createDiv('WARNING');

}

function draw() {
  if(!webcamOn) {
    button.position(windowWidth/2-100,windowHeight/2-100);

  text1.position(windowWidth/2-250,10);
  text1.style('font-size', '60px');
  text1.style('color', 'white');

  text2.position(random(windowWidth),random(windowHeight));
  text2.style('font-size', '40px');
  text2.style('color', '#0f0');

} else {
  text1.hide();
  text2.hide();
}
  button.mousePressed(webcam); //click the button to turn on webcam
}

function webcam() {
webcamOn = true;
background(220,180,245);
capture.show();
button.hide();

//if button.mousePressed(capture); //turn webcam on

  text3=createDiv('I AM WATCHING YOU');
  text3.position(windowWidth/2-400,10);
  text3.style('font-size', '80px');
  text3.style('color', 'white');


  }
