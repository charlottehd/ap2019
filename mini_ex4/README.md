# I am watching you!

![Screenshot](button1.PNG)
![Screenshot](button2.PNG)

https://cdn.staticaly.com/gl/charlottehd/ap2019/raw/master/mini_ex4/mini_ex4/empty-example/index.html


My program contains a button that says "Do not click me" and a header that says "Dont push this button". Besides is the word "Warning" randomly appearing on the screen. If you click the button all you will see is a picture of you as you, when clicking the button, actually accept your webcam to turn on. Also a text appears over the picture of you which is "I am watching you".

Codewise I have chosen to create several variables including the button and the capture. I also created my own function to make the webcam turn on when the button is clicked. For this I used a mousePressed syntax. I made a condition using "if" to prevent the webcam from turning on in the beginning of the program. 
I really struggled with the position of my elements since text isn't that easy to position in the middle of the canvas, but I think it worked out anyways.

I wished to create a program that enlightens how easy it is for hackers to watch every step you take. Buttons afford to be pushed and even though you are warned not to push it, it is very tempting, and many people will push the button anyway. By pushing this button hackers have access to your camera. I think this program shows in quite a simple way, how easy it is to get access to computers, cameras and information just by making people push a button.
In our days you always have to be aware of how you use your computer and what you click on when using it. Also it is very common to cover up webcams on laptops since that is the only way to make sure nobody's watching you.