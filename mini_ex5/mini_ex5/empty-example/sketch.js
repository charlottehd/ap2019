let img;
let c;
let load = ['I','will','never','stop','loading'];
let button;

function preload() {
  img = loadImage('picture.jpg');

}

function setup() {
  c = createCanvas(windowWidth,windowHeight);
  c.position(0,0);

  background(220,180,240);
  frameRate(0.8);

  button = createButton('try again');

    button.size(60,20);
    button.position(width/2,height/1.3);
    button.show();
    button.style('font-size', '10px');
    button.style('background-color', 'white');
    button.style('color', 'black');


  }

function draw() {
  background(220,180,240,80);
  fill(220,180,240,120);
  noStroke();
	rect(0,0,width,height);
  textThrobber(9);

}

function textThrobber(num) {
  push();
  translate(width/2,height/2);

 let cir = 360/num*(frameCount%num);

  rotate(radians(cir));
  noStroke();
  textSize(22);
  fill(255);

  text(load[frameCount%load.length],20,0);

	pop();

  button.mousePressed(picture);

}

function picture() {
  image(img,0,0);
}
