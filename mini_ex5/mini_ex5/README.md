# Revisit the past - a new throbber

![Screenshot](throbberfail.PNG)


Link:
https://cdn.staticaly.com/gl/charlottehd/ap2019/raw/master/mini_ex5/mini_ex5/empty-example/index.html



For this mini exercise I chose to remake my throbber from mini_ex3. Some of the feedback i received focused on how I could be even more consistent to my point. I originally wanted to make a throbber that said: "I will never stop loading" and it was supposed to keep loading forever. I chose my frameRate to be 2 and I was suggested to make the frameRate even lower, to make it feel like time went very slowly while waiting for my throbber. Thats why I changed it to 0.8. Another critique was that there was no interaction with my throbber, and it would be fun to have such thing. For that reason I added a button using some of the syntaxes I learned last week. I also uploaded an image which is something new that I have not done before. I struggled with uploading the image and tried a various of different ways to make the picture only appear when my button was pressed. At least i figured that i placed the picture in the wong folder... When it all worked I was very thrilled, since I didn't get any help for this excercise, but I just experimented untill it was all how I wanted it to be.

So when the button is pressed the image appears and on the image is the word "fail". It indicates that something went wrong in the process and the throbber will start all over again. This urge the user even more than before that since this thobber will never stop loading, you might as well spend your time on something else. I you try to reload the page og click the button that says "try again", nothing new will happen anyways.



After beginning to learn the basics of programming I understand why it is so important to understand it a little bit. To just know the basics it will be a lot easier to talk to programmers when designing. In relation to Anette Vee's text on coding as literacy, I really do believe that coding is literacy now more than ever before. Children in the age of 10 learn basics of coding along with math, english, history and so on. I am not saying that everyone should be experts, because then it wouldn't be much fun either, but I do believe everyone would benefit from knowing just a bit of programming.