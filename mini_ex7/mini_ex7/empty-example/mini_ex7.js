let colorx = ['#0000FF','#0033FF','#0066FF','#0099FF','#00CCFF'];
let colory = ['#FF00FF','#FF33FF','#FF66FF','#FF99FF','#FFCCFF'];
let c;
let rad = 25; // Width of the shape
let size = [20,25,30];
let xpos, ypos; // Starting position of shape

let xspeed = 14.8; // Speed of the shape
let yspeed = 8.2; // Speed of the shape

let xdirection = 1; // Left or Right
let ydirection = 1; // Top to Bottom

function setup() {
  c = createCanvas(windowWidth,windowHeight);
  c.position(0,0);
  background(0);
  // stroke();
  // noFill();
  frameRate(30);
  ellipseMode(RADIUS);
  // Set the starting position of the shape
  xpos = width / 2;
  ypos = height / 2;
}

function draw() {
// color=stroke(random(255),random(240),random(80));

// Update the position of the shape
xpos = xpos + xspeed * xdirection;
ypos = ypos + yspeed * ydirection;

// Test to see if the shape exceeds the boundaries of the screen
// If it does, reverse its direction by multiplying by -1
if (xpos > width - rad || xpos < rad) {
  xdirection *= -1;
  stroke(random(colorx));
  rad = random(size);
}
if (ypos > height - rad || ypos < rad) {
  ydirection *= -1;
  stroke(random(colory));
  rad = random(size);
}

noFill();
ellipse(xpos, ypos, rad, rad);
}
