# Generative art

![Screenshot](art.PNG)


Link:
https://cdn.staticaly.com/gl/charlottehd/ap2019/raw/master/mini_ex7/mini_ex7/empty-example/index.html


For this weeks mini exercise Mie and I worked together on creating a program consisting of generative art. We started by declaring 3 rules for our program:

1. For everytime the ellipse touches the y-axis, change the direction
2. For everytime the ellipse touches the y-axis, change the color of the stroke to a random color from the array
3. For everytime the ellipse touches the y-axis, change the size of the ellipse to a random size from the array.

We also made the same 3 rules for when the ellipse touches the x-axis, so the program would look aesthetically beautiful. We chose to make two color arrays and named them colorx and colory because we wanted the colors to differ when they touch the y- and x-axis. We chose five colors from a blue color scheme for the colorx array and 5 colors from a pink color scheme for the colory array. The sizes of the ellipses don't differ much from each other, because we still wanted it to look nice, and we found out that if we had too different sizes the canvas would be too random, and we still wanted to be able to control the randomness. Our program keeps generating new ellipses over and over again, and if you reload the page you will see that it will never be exactly the same because we implemented random.

Generative and automatism means that the program will keep running the code and doesn't really have an end. You can see that in our program, since the ellipses don't stop appearing but in the end the canvas will be painted in blue and pink ellipses. Even though this happens, it will still keep running and it will just overlap already drawn ellipses. Generative also means that it is a program made by someone, but that someone doesn't control exactly how it will turn out. The programmer just set up the rules of the program and somehow the program will never be the same.
